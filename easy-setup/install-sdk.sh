#! /bin/bash

if [ -f /usr/bin/unzip ] && [ ! -f /usr/local/share/soft/sdkman/bin/sdk ]
then    
    export SDKMAN_DIR="/usr/local/share/soft/sdkman" && curl -s "https://get.sdkman.io" | bash 
    chown -R root:lazydevelopers /usr/local/share/soft
    chmod -R 774 /usr/local/share/soft/sdkman    
    source /usr/local/share/soft/sdkman/bin/sdkman-init.sh
else
    echo " Either unzip missing or sdkman already installed"
    exit 1
fi


if [ -f /usr/local/share/soft/sdkman/bin/sdkman-init.sh ]
then
    export SDKMAN_DIR="/usr/local/share/soft/sdkman"
    source /usr/local/share/soft/sdkman/bin/sdkman-init.sh

    sdk current java | grep '8.0.201-oracle'
    if [ $? -eq 1 ]
    then
        sdk install java 8.0.201-oracle
    fi

    sdk current maven | grep 'Not using any version'
    if [ $? -eq 0 ]
    then
        sdk install maven
    fi

    sdk current groovy | grep 'Not using any version'
    if [ $? -eq 0 ]
    then
        sdk install groovy
    fi

    sdk current gradle | grep 'Not using any version'
    if [ $? -eq 0 ]
    then
        sdk install gradle
    fi

    sdk current springboot | grep 'Not using any version'
    if [ $? -eq 0 ]
    then
        sdk install springboot
    fi
fi

#finally set group info
chown -R root:lazydevelopers /usr/local/share/soft
echo "Add 'source /usr/local/share/soft/sdkman/bin/sdkman-init.sh' to your profile"

