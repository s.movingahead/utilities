#!/bin/sh


# Script to install basic tools
# To be run as sudo fresh-dev-setup.sh


#export SDKMAN_CANDIDATES_DIR=/usr/local/share/sdkman/candidates
mkdir -p /usr/local/share/soft
groupadd -g 9900 lazydevelopers

chown root:lazydevelopers /usr/local/share/soft
chmod 774 /usr/local/share/soft

apt update
apt install -y apt-transport-https ca-certificates curl software-properties-common apt-transport-https zip unzip gcc g++ make wget


if [ -f /usr/bin/docker ]
then
    echo 'Docker already installed'
    docker --version
else
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get -y install docker-ce
fi
    
if [ -f /usr/bin/nodejs ] && [ -f /usr/bin/npm ]
then
    echo "Node and npm already installed"
else
    curl -sL https://deb.nodesource.com/setup_10.x |  bash -
    apt-get -y install nodejs
fi



if [ -f /usr/local/bin/insomnia ]
then
    echo 'Insomnia already installed'
else
    echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" |  tee -a /etc/apt/sources.list.d/insomnia.list
    wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc |  apt-key add -
    apt-get update
    apt-get -y install insomnia
fi



if [ ! -f /usr/bin/code ]
then
    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg

    install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
    sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

    apt-get -y install code
fi

if [ -d /usr/local/share/soft/eclipse ]
then
    echo 'Eclipse installed already'
else
    echo 'Downloading eclipse'
    wget -O eclipse-jee-2018-12-R-linux-gtk-x86_64.tar.gz http://ftp.jaist.ac.jp/pub/eclipse/technology/epp/downloads/release/2018-12/R/eclipse-jee-2018-12-R-linux-gtk-x86_64.tar.gz

    tar -zxf eclipse-jee-2018-12-R-linux-gtk-x86_64.tar.gz -C /usr/local/share/soft

    chown -R root:lazydevelopers /usr/local/share/soft/eclipse

    ln -s /usr/local/share/soft/eclipse/eclipse /usr/bin/eclipse

    touch /usr/share/applications/eclipse.desktop
    echo "[Desktop Entry]" > /usr/share/applications/eclipse.desktop
    echo "Encoding=UTF-8" >> /usr/share/applications/eclipse.desktop
    echo "Name=Eclipse 4.10" >> /usr/share/applications/eclipse.desktop
    echo "Comment=Comment=Eclipse R" >> /usr/share/applications/eclipse.desktop
    echo "Exec=/usr/bin/eclipse" >> /usr/share/applications/eclipse.desktop
    echo "Icon=/usr/eclipse/icon.xpm" >> /usr/share/applications/eclipse.desktop
    echo "Terminal=false" >> /usr/share/applications/eclipse.desktop
    echo "Type=Application" >> /usr/share/applications/eclipse.desktop
    echo "StartupNotify=false" >> /usr/share/applications/eclipse.desktop
    echo "Categories=Programming;Development;IDE;Java;" >> /usr/share/applications/eclipse.desktop
fi


