# easy-setup 

Using fresh-dev-setup.sh, we can install below softwares easily

    [*] sdkman
    [*] java
    [*] maven
    [*] groovy
    [*] gradle
    [*] eclipse
    [*] spring boot
    [*] insomnia    -- Rest client (POSTMAN alternative)

1. Clone the repo.
2. cd utilities;chmod +x fresh-dev-setup.sh
3. sudo ./fresh-dev-setup.sh
4. sudo usermod -aG lazydevelopers $USER
5. sudo ./install-sdk.sh
